import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent {

  public vehicles: Vehicle[];
  public vehicle: Vehicle;
  public showForm = false;
  public saveId = null;
  public hideButton = false;
  

  constructor(public http: HttpClient, @Inject('BASE_URL') public baseUrl: string) {
    this.refresh();
  }

  refresh(){
    this.http.get<Vehicle[]>(this.baseUrl + 'api/vehicles').subscribe(result => {
      this.vehicles = result;
      this.vehicle = {
        color: "#000000",
        manufacturer: "",
        year: 2020,
        mileage: 0
      };
      this.showForm = false;
      this.hideButton = false;
    }, error => console.error(error));
  }

  save() {
    this.http.post(this.baseUrl + 'api/vehicles', this.vehicle).subscribe(() => {
      this.refresh();
    }, error => console.error(error));
  }

  delete(id){
    this.http.delete(this.baseUrl + 'api/vehicles/' + id).subscribe(() => {
      this.refresh();
    }, error => console.error(error));
    this.hideButton = false;
  }

  update(id){
    this.http.patch(this.baseUrl + 'api/vehicles/' + id, [

      {
        "op": "add", "path": "/color", "value": this.vehicle.color
      },
      {
        "op": "add", "path": "/manufacturer", "value": this.vehicle.manufacturer
      },
      {
        "op": "add", "path": "/year", "value": this.vehicle.year
      },
      {
        "op": "add", "path": "/mileage", "value": this.vehicle.mileage
      }

    ]).subscribe(() => {
      this.refresh();
    }, error => console.error(error));  
  }

  edit(info){
      this.showForm = true;
      this.hideButton = true;
      var id = parseInt(info[0]);
      this.vehicle.color = info[1];
      this.vehicle.manufacturer = info[3];
      this.vehicle.year = info[2];
      this.vehicle.mileage = info[4];
      this.saveId = id;
  }
}
interface Vehicle {
  color: string;
  year: number;
  manufacturer: string;
  mileage: number;
}
